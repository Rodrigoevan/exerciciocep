package br.com.mastertech.usuario.client;

import br.com.mastertech.usuario.model.CEP;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep")
public interface ClientCEP {

    @GetMapping("/cep/{cep}")
    CEP buscacep(@PathVariable String cep);

}
