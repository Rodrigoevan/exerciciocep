package br.com.mastertech.usuario.controller;

import br.com.mastertech.usuario.model.CEP;
import br.com.mastertech.usuario.model.Usuario;
import br.com.mastertech.usuario.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BuscaController {

    @Autowired
    UsuarioService usuarioService;

    @GetMapping("/usuario/{nome}/{cep}")
    public Usuario buscar(@PathVariable String nome, @PathVariable String cep){
        CEP cepObjeto = usuarioService.buscaCep(cep);
        Usuario usuario = new Usuario();
        usuario.setNome(nome);
        usuario.setEndereco(cepObjeto.getCep() + "-" + cepObjeto.getBairro());
        return usuario;
    }

}
