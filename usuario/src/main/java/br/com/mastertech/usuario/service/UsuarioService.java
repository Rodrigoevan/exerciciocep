package br.com.mastertech.usuario.service;

import br.com.mastertech.usuario.client.ClientCEP;
import br.com.mastertech.usuario.model.CEP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    private ClientCEP clientCEP;


    public CEP buscaCep(String cep) {
        CEP cepObjeto = clientCEP.buscacep(cep);
        return cepObjeto;
    }
}
