package br.com.mastertech.cep.controller;

import br.com.mastertech.cep.model.CEP;
import br.com.mastertech.cep.service.CEPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CEPController {

    @Autowired
    private CEPService cepService;

    @GetMapping("/cep/{cep}")
    public CEP buscarCep(@PathVariable String cep){
        return cepService.findByCep(cep);

    }
}
