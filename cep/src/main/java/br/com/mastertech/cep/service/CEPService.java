package br.com.mastertech.cep.service;

import br.com.mastertech.cep.client.CEPFeign;
import br.com.mastertech.cep.model.CEP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

@Service
public class CEPService {

    @Autowired
    CEPFeign cepFeign;

    @NewSpan(name = "buscar-cep")
    public CEP findByCep(@SpanTag("num cep")String cep) {
        return cepFeign.findByCep(cep);
    }
}
