package br.com.mastertech.cep.client;

import br.com.mastertech.cep.model.CEP;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep", url = "https://viacep.com.br/ws/")
public interface CEPFeign {

    @GetMapping("{cep}/json")
    public CEP findByCep(@PathVariable String cep);
}
